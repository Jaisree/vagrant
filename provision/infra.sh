# INSTALL DOCKER (REQUIRED FOR SKAFFOLD)
     curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
     sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
     sudo apt-get update
     sudo apt-get install -y docker-ce 
     sudo usermod -aG docker vagrant

# INSTALL DOCKER-COMPOSE

     sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
     sudo chmod +x /usr/local/bin/docker-compose

# NODEJS
    sudo apt-get update && sudo apt-get -y upgrade
    curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh
    sudo bash nodesource_setup.sh
    sudo apt-get install nodejs -y
    sudo apt-get install build-essential -y

# INSTALL YARN

    curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
    sudo apt update && sudo apt install yarn

# PRISMA CLI FOR PRISMA SERVER
    sudo npm install -g prisma

# INSTALL PYTHON AND PIP
    sudo add-apt-repository ppa:deadsnakes/ppa
    sudo apt update
    sudo apt install python3.6 -y
    
# INSTALL VIRTUAL ENVIRONMENT
    sudo apt-get install python3-pip -y
    sudo pip3 install virtualenv 

# INSTALL HEROKU-CLI
    sudo snap install --classic heroku     
